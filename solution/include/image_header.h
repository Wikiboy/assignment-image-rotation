#ifndef LAB1_1_IMAGE_HEADER_H
#define LAB1_1_IMAGE_HEADER_H
#include "image.h"
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_INVALID_FORMAT,
    READ_ERROR,
    READ_INVALID_COUNT_OF_ARGS
};

/*  serializer   */
enum write_status  {
    WRITE_OK = 0,
    WRITE_INVALID_FORMAT,
    WRITE_INVALID_SIGNATURE,
    WRITE_ERROR,
    WRITE_INVALID_IMAGE
};
enum make_status  {
    MAKE_OK = 0,
    MAKE_ERROR
};
enum open_file_status  {
    OK = 0,
    INVALID_SIGNATURE
};
static const char *const read_status_to_string[] = {
        [READ_OK] = "READ_OK",
        [READ_INVALID_SIGNATURE] = "READ_INVALID_SIGNATURE",
        [READ_INVALID_HEADER] = "READ_INVALID_HEADER",
        [READ_INVALID_FORMAT] = "READ_INVALID_FORMAT",
        [READ_ERROR] = "READ_ERROR",
        [READ_INVALID_COUNT_OF_ARGS] = "READ_INVALID_COUNT_OF_ARGS"};
static const char *const write_status_to_string[] = {
        [WRITE_OK] = "WRITE_OK",
        [WRITE_INVALID_FORMAT] = "WRITE_INVALID_FORMAT",
        [WRITE_INVALID_SIGNATURE] = "WRITE_INVALID_SIGNATURE",
        [WRITE_ERROR] = "WRITE_ERROR",
        [WRITE_INVALID_IMAGE] = "WRITE_INVALID_SIGNATURE"};
static const char *const make_status_to_string[] = {
        [MAKE_OK] = "MAKE_OK",
        [MAKE_ERROR] = "MAKE_ERROR"};
static const char *const open_file_status_to_string[] = {
        [OK] = "OK",
        [INVALID_SIGNATURE] = "INVALID_SIGNATURE"};
enum read_status from_picture(FILE* in, const char* name_of_image, struct image * img);
enum write_status to_picture(FILE* out, const char* name_of_image, struct image * img);
enum open_file_status check_file(FILE** f, const char* name_of_image, char * format);
char * find_format(const char * name_of_image);
#endif //LAB1_1_IMAGE_HEADER_H
