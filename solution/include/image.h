#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#include <inttypes.h>
#include <stdio.h>
#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel** data;
};
#pragma pack(pop)

void image_free( struct image* image);
struct image transform(struct image img, struct image (f)(struct image));
#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
