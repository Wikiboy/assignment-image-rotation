#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H
#include "image.h"
#include "image_header.h"
#include "inttypes.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

#define BI_SIZE 40
#define BI_PLANES 1
#define BI_Y_PELS_PER_METER 2834
#define BI_X_PELS_PER_METER 2834
#define BI_BIT_COUNT 24
#define BF_TYPE 19778


enum write_status to_bmp( FILE* out, struct image * img);
enum read_status from_bmp( FILE* in, struct image * img);
enum write_status image_write( struct image * image, FILE* f);
enum read_status image_read( struct image * image, FILE* f);
uint64_t around(uint64_t t);
enum make_status make_bmp_header(struct bmp_header* header, struct image * img);
enum make_status is_image_correct(struct image * img);
enum read_status is_bmp_header_correct(struct bmp_header* header);
#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_HEADER_H
