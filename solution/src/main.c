#include "bmp.h"
#include "image_header.h"
#include "rotate.h"
#include <stdio.h>
#include <string.h>

int main( int argc, char** argv ) {
    FILE* f = NULL;
    struct image img;
    if(argc != 3){
        printf("%d \n", argc);
        fprintf(stderr, "%s \n", read_status_to_string[READ_INVALID_COUNT_OF_ARGS]);
        //printf("%s \n", read_status_to_string[READ_INVALID_COUNT_OF_ARGS]);
        return -1;
    }
    else {
        enum read_status read = from_picture(f, argv[1], &img);
        //if(strcmp(read_status_to_string[read], read_status_to_string[0]) == 0) {
        if(read == READ_OK) {
            struct image new_image = transform(img, &rotate);
            enum write_status write = to_picture(f, argv[2], &new_image);
            if(write == WRITE_OK){
            //if(strcmp(write_status_to_string[write], write_status_to_string[0]) == 0){
                return 0;
            }
            else {
                fprintf(stderr, "%s \n", write_status_to_string[write]);
                //printf("%s \n", write_status_to_string[write]);
                image_free(&new_image);
            }
        }
        else {
            fprintf(stderr, "%s \n", read_status_to_string[read]);
            //printf("%s \n", read_status_to_string[read]);
        }
    }
    return -1;
}
