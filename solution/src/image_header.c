#include "bmp.h"
#include <stdio.h>
#include <string.h>

enum open_file_status check_file(FILE** f, const char* name_of_image, char * format){
    if((*f = fopen(name_of_image,format)) == NULL) {
        return INVALID_SIGNATURE;
    }
    return OK;
}
char * find_format(const char * name_of_image){
    const char sep [1]=".";
    char *format = " ";
    char *format_not_null = " ";
    format = strtok ((char *)name_of_image,sep);
    while (format != NULL) {
        format_not_null = format;
        format = strtok(NULL, sep);
    }
    return format_not_null;
}
enum read_status from_picture(FILE* in, const char* name_of_image, struct image * img){
    if(check_file(&in, name_of_image, "rb") == INVALID_SIGNATURE) {
    //if(strcmp(open_file_status_to_string[check_file(&in, name_of_image, "rb")], open_file_status_to_string[0]) != 0) {
        return READ_INVALID_SIGNATURE;
    }
    if(strcmp(find_format(name_of_image), "bmp") == 0){
        enum read_status status = from_bmp(in, img);
        fclose(in);
        return status;
    } else {
        fclose(in);
        return  READ_INVALID_FORMAT;
    }
}
enum write_status to_picture(FILE* out, const char* name_of_image, struct image * img){
    if(check_file(&out, name_of_image, "wb") == INVALID_SIGNATURE) {
    //if(strcmp(open_file_status_to_string[check_file(&out, name_of_image, "wb")], open_file_status_to_string[1]) == 0) {
        return WRITE_INVALID_SIGNATURE;
    }
    if(strcmp(find_format(name_of_image), "bmp") == 0){
        enum write_status status = to_bmp(out, img);
        fclose(out);
        return status;
    } else {
        fclose(out);
        return  WRITE_INVALID_FORMAT;
    }
}
