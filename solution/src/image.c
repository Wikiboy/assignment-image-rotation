#include "image.h"
#include <malloc.h>

void image_free( struct image* image) {
    struct pixel **array = image->data;
    if (array) {
        for (uint64_t i = 0; i < image->height; i++) {
            free(array[i]);
        }
        free(array);
    }
}
struct image transform(struct image img, struct image (f)(struct image)){
    return f(img);
}
