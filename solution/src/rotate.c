#include "mm_malloc.h"
#include "rotate.h"

struct image rotate( struct image source ){
    struct image new_image = {0};
    new_image.height = source.width;
    new_image.width = source.height;
    new_image.data = (struct pixel**)malloc(sizeof (struct pixel*)*new_image.height);
    for(uint64_t i = 0; i < new_image.height; i++) {
        new_image.data[i] = (struct pixel*)malloc((new_image.width)*sizeof(struct pixel));
    }
    for(uint64_t i = 0; i < new_image.height; i++) {
        for (uint64_t l = 0; l < new_image.width; l++){
            new_image.data[i][new_image.width - 1 - l] = source.data[l][i];
        }
    }
    image_free(&source);
    return new_image;
}
