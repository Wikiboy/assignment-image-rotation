#include "bmp.h"
#include <malloc.h>

uint64_t around(uint64_t t){
    return 4 - (t * sizeof(struct pixel)) % 4;
}
enum write_status image_write( struct image * image, FILE* f) {
    size_t padding = around(image->width);
    for(uint64_t i = 0; i < image->height; i++) {
        for (uint64_t l = 0; l < image->width; l++){

            if(fwrite(&(image->data[i][(int64_t)l]),sizeof(struct pixel),1,f) != 1){
                return WRITE_ERROR;
            }
        }
        if (padding != 0) {
            if(fseek(f, padding, SEEK_CUR) != 0){
                return WRITE_ERROR;
            }
        }
    }
    image_free(image);
    return WRITE_OK;
}
enum read_status image_read( struct image * image, FILE* f) {
    image->data = (struct pixel**)malloc(sizeof (struct pixel*)*image->height);
    size_t padding = around(image->width);
    for(uint64_t i = 0; i < image->height; i++) {
        if (image->width != 0) {
            image->data[i] = (struct pixel *) malloc((image->width) * sizeof(struct pixel));
            for (uint64_t l = 0; l < image->width; l++) {
                //printf("%lu \n", fread(&(image->data[i][(int64_t) l]), sizeof(struct pixel), 1, f));
                //printf("%lu \n", sizeof(struct pixel));
                if(fread(&(image->data[i][(int64_t) l]), sizeof(struct pixel), 1, f) != 1){
                    return READ_ERROR;
                }
            }
            if (padding != 0) {
                if(fseek(f, padding, SEEK_CUR) != 0){
                    return READ_ERROR;
                }
            }
        }
    }
    return READ_OK;
}
enum read_status is_bmp_header_correct(struct bmp_header* header){
    if(header->bfType != 19778 || header->biBitCount != 24) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}
enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header hdBMP = {0};
    if(fread(&hdBMP,sizeof(hdBMP),1,in) != 1){
        return READ_ERROR;
    }
    img -> height = hdBMP.biHeight;
    img -> width = hdBMP.biWidth;
    if(is_bmp_header_correct(&hdBMP) == READ_OK) {
        return image_read(img, in);
    }
    return READ_INVALID_HEADER;
}
enum make_status is_image_correct(struct image * img){
    if(img->height == 0 || img->width == 0){
        return MAKE_ERROR;
    }
    return MAKE_OK;
}
enum make_status make_bmp_header(struct bmp_header* header, struct image * img){
    enum make_status status = is_image_correct(img);
    header->biHeight = img -> height;
    header->biWidth = img -> width;
    header->biSize = BI_SIZE;
    header->biPlanes = BI_PLANES;
    header->biSizeImage = header->biHeight * header->biWidth * sizeof(struct pixel) + (header->biHeight) * around(header->biWidth);
    header->bOffBits = sizeof(struct bmp_header);
    header->biYPelsPerMeter = BI_Y_PELS_PER_METER;
    header->biXPelsPerMeter = BI_X_PELS_PER_METER;
    header->biBitCount = BI_BIT_COUNT;
    header->bfileSize = header->biSizeImage + header->bOffBits;
    header->bfType = BF_TYPE;
    return status;
}

enum write_status to_bmp( FILE* out, struct image * img){
    struct bmp_header hdBMP = {0};
    if(make_bmp_header(&hdBMP, img) == MAKE_ERROR){
        return WRITE_INVALID_IMAGE;
    }
    if(fwrite(&hdBMP,sizeof(hdBMP), 1,out) == 1){
        return image_write(img, out);
    }
    return WRITE_ERROR;
}
